import os
import pandas as pd
import numpy as np
import scipy.optimize
from matplotlib import pyplot as plt


def zip_portfolio(rates, shares):
    # select the same columns from rates
    shares = shares[list(rates.columns)].reindex(rates.columns, axis=1)

    # check if columns list are equal
    rate_shares = pd.DataFrame()
    if np.array_equal(shares.columns, rates.columns.values):
        # multiply each stock ror by its share
        rate_shares = rates.mul(shares.values.tolist()[0])
        rate_shares['pf_ror'] = rate_shares.sum(axis=1)
    else:
        print('Column lists do not match, verify the files!')

    return rate_shares['pf_ror']


def f(x):
    p = 1 / samples_count
    string_equation = ""
    for i, r in enumerate(pf_results):
        if i == 0:
            string_equation = string_equation + "{}*np.log(1+x*{})".format(p, r)
        else:
            string_equation = string_equation + " + {}*np.log(1+x*{})".format(p, r)
    # print(string_equation)
    return eval(string_equation)


if __name__ == '__main__':
    fh_rates = pd.read_csv(os.path.join(os.getcwd(), "copula_scenarios", 'tstudent_fh_rates.csv'), delimiter=';')
    pf_shares = pd.read_csv(os.path.join(os.getcwd(), "model_results", 'dual_values_tstudent.csv'),
                        usecols=['tickers', 'dual_values']).transpose()
    # replace columns names in shares with tickers
    pf_shares.columns = list(pf_shares.iloc[0])
    pf_shares.drop(index='tickers', inplace=True)

    pf_results = zip_portfolio(fh_rates, pf_shares)
    samples_count = len(pf_results)
    max_x = scipy.optimize.fmin(lambda x: -f(x), 0, ftol=1e-10)
    print('maximum value x: {}'.format(max_x))

    space = np.linspace(0, 10, 50)
    fun = pd.DataFrame(space, columns=['x'])
    fun['f'] = fun['x'].apply(f)
    # print(fun)
    fc = scipy.optimize.broyden1(f, 1, f_tol=1e-10)
    print('result of optimization: {}'.format(fc))

    plt.plot(fun['x'].values, fun['f'].values)
    plt.xlabel('f')
    plt.ylabel('G(f)', rotation=0, labelpad=10)
    plt.grid(1)
    plt.show()
