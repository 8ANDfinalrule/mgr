
import yfinance as yf
import pandas as pd
import os
from pandas.tseries.offsets import MonthBegin
import signal
import time


class TimeOutException(Exception):
    pass


def alarm_handler(signum, frame):
    print("ALARM signal received")
    raise TimeOutException()


def align_to_period(df):
    df = df[df['Adj Close'].notnull()].sort_index(ascending=False)
    df['prev_dt'] = df.index
    df = df[~df.index.to_period('Q').duplicated()]
    # drop unfinished quarters
    if not pd.to_datetime(df.index.values[1]).is_quarter_end:
        df.drop(1)
    df.sort_index(inplace=True)
    df.index = df.index.to_period('Q').to_timestamp('Q')
    return df


def download_stocks_data(calc_yr_str):
    data = pd.read_html('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies')
    stocks = data[0].iloc[:, 0]
    stocks = stocks.apply(lambda x: x.replace('.', '-'))
    wd = os.getcwd()
    with open(os.path.join(wd, "sp500.csv"), 'w') as file:
        file.write(stocks.to_csv())
    for ticker in stocks:
        print('downloading data for {} ...'.format(ticker))
        signal.signal(signal.SIGALRM, alarm_handler)
        signal.alarm(5)
        finished = False
        while not finished:
            try:
                history = yf.download(ticker, interval='1d')
                finished = True
            except TimeOutException as ex:
                print('timeout')
        signal.alarm(0)

        print('data for {} downloaded\n'.format(ticker))
        history_aligned = align_to_period(history)
        with open(os.path.join(wd, calc_yr_str, "data_original", ticker+'.csv'), 'w') as file:
            file.write(history_aligned.to_csv())


# download_stocks_data(calc_yr_str="2021")

dt = yf.download('A', interval='1mo')
with open(os.path.join(os.getcwd(), 'tmp.csv'), 'w') as file:
    file.write(dt.to_csv())
print(dt)
