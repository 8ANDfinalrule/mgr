
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

CALC_YEAR_STR = "2021"
INPUT_DATA_FOLDER_NM = "data_original"
OUTPUT_DATA_FOLDER_NM = "data_prepared"
pd.set_option('display.max_rows', 1000)


def check_continuity(df):
    df['TD'] = (df['Date'].diff(1) / np.timedelta64(3, 'M')).round(0)
    diff_mask = df['TD'].map(lambda x: x != 1)

    stock_e = df[df['TD'].notnull()].loc[diff_mask]
    if stock_e.shape[0] > 0:
        print(df)
        return False
    else:
        return True


def check_start_date(df, start_date):
    date = pd.to_datetime(df['Date'].min())
    diff = pd.Timedelta(date - start_date)
    if diff.days > 0:
        return True
    else:
        return False


def prepare_data():
    cwd = os.getcwd()
    sp500_path = os.path.join(cwd, "sp500.csv")
    stock_list = pd.read_csv(sp500_path)
    stock_list = stock_list['Symbol']
    short_history = []
    print('converting stocks ...')
    for s in stock_list:
        path = os.path.join(cwd, CALC_YEAR_STR, INPUT_DATA_FOLDER_NM, s + '.csv')
        stock_data = pd.read_csv(path)
        stock_data = stock_data[stock_data['Adj Close'].notnull()]

        # convert string to pandas datetime
        stock_data['Date'] = pd.to_datetime(stock_data['Date'])

        # keep only quarters start dates
        quarter_mask = stock_data['Date'].map(lambda x: x.is_quarter_end)
        # print(stock_data)
        # print('\n*********\n')
        stock_data = stock_data[quarter_mask].sort_values('Date')
        # print(stock_data)
        if check_start_date(stock_data, pd.to_datetime("1990-01-01")):
            short_history.append([s, stock_data['Date'].min().strftime('%Y-%m-%d')])
            continue

        # continuity check can be skipped
        # if check_continuity(stock_data) and stock_data.shape[0] != 0:
        if stock_data.shape[0] != 0:

            stock_data = stock_data[['Date', 'Close', 'Adj Close']]
            # calculate rate of return
            stock_data['RoR'] = (stock_data['Adj Close'] - stock_data['Adj Close'].shift()) \
                                / stock_data['Adj Close'].shift()
            # delete first row with null RoR
            stock_data = stock_data[stock_data['RoR'].notnull()]
            with open(os.path.join(cwd, CALC_YEAR_STR, OUTPUT_DATA_FOLDER_NM, s + '.csv'), 'w') as file:
                file.write(stock_data.reset_index(drop=True).to_csv())
        else:
            print('Lack of continuity in '+s)

    with open(os.path.join(cwd, OUTPUT_DATA_FOLDER_NM, 'short_history.csv'), 'w') as file:
        for line in short_history:
            file.write(' '.join(line)+'\n')


prepare_data()


# DRAW CHART
symbol = "AAPL"
path = os.path.join(os.getcwd(), CALC_YEAR_STR, OUTPUT_DATA_FOLDER_NM, symbol + '.csv')
stock = pd.read_csv(path)
#
ax = plt.gca()
# previously was also: & (stock['Date'] < '2020-04-01')
stock = stock[(stock['Date'] > '1990-01-01')]

# stock.plot(x='Date', y='Adj Close', kind='line', color='green', ax=ax)
# stock.plot(x='Date', y='RoR', kind='line', color='blue', ax=ax)
# plt.axhline(y=0, color='r', linestyle='--', linewidth=2)
# plt.show()
