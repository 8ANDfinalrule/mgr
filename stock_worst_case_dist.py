
import pandas as pd
import os
import matplotlib.pyplot as plt
from numpy import random
import time

CALC_YEAR_STR = "2021"


def quantile(df, col_name):
    frame = df[col_name].copy().dropna()
    frame = frame.sort_values().reset_index()
    frame['p'] = frame.index / (len(frame) - 1)
    return frame


def generate_time_frames(df, col_name, frame_length):
    no_frames = len(df) - frame_length + 1
    frames = pd.DataFrame()
    if no_frames > 0:
        for f in range(no_frames):
            current_frame = df[f:f+frame_length]
            frames["frame{}".format(f)] = current_frame[col_name].tolist()
    else:
        print('No time frames created, length demanded larger than data length')
    return frames


def worst_case_dist(df, col_name, frame_length=50):
    time_frames = generate_time_frames(df, col_name, frame_length)
    quantile_funcs = pd.DataFrame()
    for i in range(time_frames.shape[1]):
        current_quant = quantile(time_frames, "frame{}".format(i))
        if i == 0:
            quantile_funcs['p'] = current_quant['p'].copy()
        quantile_funcs["quant{}".format(i)] = current_quant["frame{}".format(i)].copy()
        # plt.plot('p', "quant{}".format(i), data=quantile_funcs, linewidth=1)
    quantile_funcs['worst'] = quantile_funcs.drop(columns="p").min(axis=1)
    # print(quantile_funcs[['p', 'worst']])
    # plt.plot('p', 'worst', data=quantile_funcs, linewidth=5)
    # plt.show()
    # plt.clf()
    return quantile_funcs


def expected_value(df, col):
    return df[col].sum() / df.shape[0]


def generate_worst_dist(f_length=32, flg_write_output=1, flg_draw_chart=1, out_of_sample_dt='2015-01-01'):
    cwd = os.getcwd()
    sp500_path = os.path.join(cwd, "sp500.csv")
    stock_list = pd.read_csv(sp500_path)
    stock_list = stock_list['Symbol']
    dist_expected_returns = []
    if not os.path.exists(os.path.join(cwd, CALC_YEAR_STR, "worst_case_distributions")):
        os.mkdir(os.path.join(cwd, CALC_YEAR_STR, "worst_case_distributions"))

    for s in stock_list:
        path = os.path.join(cwd, CALC_YEAR_STR, "data_prepared", s+'.csv')
        if os.path.exists(path):
            print("Generating worst case distribution for {}".format(s))
            stock_data = pd.read_csv(path, usecols=['Date', 'RoR'])
            stock_data = stock_data[(stock_data['Date'] < out_of_sample_dt)]
            worst_case = worst_case_dist(stock_data, "RoR", frame_length=f_length)
            ev = expected_value(worst_case, 'worst')
            dist_expected_returns.append([s, str(ev)])
            if ev > 0:
                print("Expected value of {}'s RoR for worst case dist: {:.3%}".format(s, ev))
                if flg_write_output:
                    with open(os.path.join(cwd, CALC_YEAR_STR, "worst_case_distributions", s + '.csv'), 'w') as file:
                        file.write(worst_case[['p', 'worst']].to_csv())
            if flg_draw_chart:
                plt.plot('p', 'worst', data=worst_case, linewidth=1, c=random.rand(3,), label=s)
                plt.draw()

    # output all expected ror
    with open(os.path.join(cwd, CALC_YEAR_STR, 'worst_case_distributions', 'wcd_all_expected_ror.csv'), 'w') as file:
        for line in dist_expected_returns:
            file.write(';'.join(line) + '\n')

    # output positive expected ror
    with open(os.path.join(cwd, CALC_YEAR_STR, 'worst_case_distributions', 'wcd_pstv_expected_ror.csv'), 'w') as file:
        for line in dist_expected_returns:
            if float(line[1]) > 0:
                file.write(';'.join(line) + '\n')

    if flg_draw_chart:
        plt.legend()
        plt.title('Worst case distributions')
        plt.show()


generate_worst_dist(f_length=32, flg_write_output=1, flg_draw_chart=0, out_of_sample_dt='2012-01-01')
