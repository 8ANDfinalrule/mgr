
import os
import pandas as pd
import time


def generate_model_file(beta=0.05
                    , xg=0.1
                    , uzero=0.001
                    , samples_nrows=None
                    , scenarios_path=os.path.join(os.getcwd(), "copula_scenarios", "clayton_rates.csv")
                    , mean_rates_path=os.path.join(os.getcwd(), "worst_case_distributions", "wcd_pstv_expected_ror.csv")
                    , output_file=os.path.join(os.getcwd(), "model_results", "model.lp")):
    start_time = time.time()
    print('Reading input file ...')

    # for testing partial input file
    # generated_samples = pd.read_csv(scenarios_path, delimiter=";", nrows=samples_nrows, usecols=['ABT', 'AME'])
    generated_samples = pd.read_csv(scenarios_path, delimiter=";", nrows=samples_nrows)
    nvars = len(generated_samples.columns)
    nrows = len(generated_samples.index)

    # read mean rates of return from wcd
    mean_rors = pd.read_csv(mean_rates_path, index_col=0, delimiter=';', header=None, names=['ticker', 'mean'])
    mean_rors = mean_rors.reindex(generated_samples.columns)

    print('Generating model file ...')
    objective = "q - {} s".format(uzero)
    constraints = ""
    dual_bounds = " c{}: ".format(nvars+1)
    bounds = ""
    upper_bound = (1 / nrows) / beta

    for stock_id in range(nvars):
        # append to objective
        objective += " + {} v{}".format(xg, stock_id + 1)

        # append to constraints
        constraints += " c{}: q - {} s".format(stock_id + 1, mean_rors.iloc[stock_id, 0])

        for i, value in enumerate(generated_samples.iloc[:, stock_id].values, start=1):
            if value < 0:
                constraints += " + {} u{}".format(abs(value), i)
            else:
                constraints += " - {} u{}".format(value, i)
            if i % 3 == 0:
                constraints += '\n\t'
        constraints += ' + v{} >= 0\n'.format(stock_id+1)

    print('Generating dual constraints ...')
    for row_id in range(nrows):
        if row_id == 0:
            dual_bounds += "u{}".format(row_id+1)
        else:
            dual_bounds += " + u{}".format(row_id+1)
        if row_id > 0 and row_id % 10 == 0:
            dual_bounds += '\n\t'
    dual_bounds += " = 1\n ".format(nvars+2)

    print('Generating bounds ...')
    for row_id in range(nrows):
        bounds += " 0 <= u{} <= {}\n".format(row_id+1, upper_bound)
    for var_id in range(nvars):
        bounds += " 0 <= v{}\n".format(var_id+1)
    bounds += " 0 <= s\n q free\n"

    print('Writing output file ...')
    with open(output_file, 'w') as file:
        file.write("\nMinimize\n obj: "+objective+"\n")
        file.write("Subject To\n"+constraints+dual_bounds)
        file.write("Bounds\n"+bounds)
        file.write("End")
    print('Model file created successfully')
    print("Elapsed time:\n--- {:.2f} seconds ---\n".format(time.time() - start_time))


# generate_model_file(beta=0.05, xg=0.1, uzero=0.01, samples_nrows=100)
