import time
import cplex
import pandas as pd
import os
from create_model_file import generate_model_file

OUTPUT_MODEL_PATH = '//model_results/solution.lp'


def optimize_model(beta=0.05
                , xg=0.05
                , uzero=0.01
                , samples_nrows=None
                , copula_name='clayton'
                , mean_rates_path=os.path.join(os.getcwd(), "wcd_pstv_expected_ror.csv")):
    scenarios_path = os.path.join(os.getcwd(), "copula_scenarios", "{}_rates.csv".format(copula_name))
    model_file_path = os.path.join(os.getcwd(), "model_results", "model_{}.lp".format(copula_name))
    duals_path = os.path.join(os.getcwd(), 'model_results', 'dual_values_{}.csv'.format(copula_name))

    # generate file with the model
    # beta - cvar level
    # xg - max portoflio share percentage
    # uzero - min return rate
    generate_model_file(beta=beta, xg=xg, uzero=uzero, samples_nrows=samples_nrows
                        , scenarios_path=scenarios_path, mean_rates_path=mean_rates_path, output_file=model_file_path)

    c = cplex.Cplex(model_file_path)

    start_time = time.time()
    c.solve()
    print("Elapsed time:\n--- {:.2f} seconds ---\n".format(time.time() - start_time))
    print("Status of the solution: "+c.solution.status[c.solution.get_status()]+'\n')

    samples_path = os.path.join(os.getcwd(), "copula_scenarios", "clayton_rates.csv")
    tickers = pd.read_csv(samples_path, delimiter=";", nrows=1)
    tickers_aligned = list(tickers.columns)
    tickers_aligned.extend('.')
    #
    pd.options.display.float_format = '{:,.8f}'.format
    result = pd.DataFrame(columns=['tickers', 'dual_values'])
    result['tickers'] = tickers_aligned
    result['dual_values'] = c.solution.get_dual_values()
    print("Solution value: "+str(c.solution.get_objective_value()))
    print('Duals:')
    print(result)

    with open(duals_path, 'w') as file:
        print('Writing results to file ...')
        file.write(result.round(decimals=6).to_csv())

    # with open(os.path.join(os.getcwd(), 'optimization_results.csv'), 'w') as file:
    #     file.write(result.to_csv())
    #     file.write(str(c.solution.get_values()))
    # c.write(OUTPUT_MODEL_PATH)


# optimize model for clayton copula
optimize_model(beta=0.05
                , xg=0.06
                , uzero=0.01
                , samples_nrows=None
                , copula_name='clayton')

