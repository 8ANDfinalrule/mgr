import numpy as np
import pandas as pd
import os
from foster_hart import zip_portfolio
from matplotlib import pyplot as plt

# pd.options.display.float_format = '{:.4f}'.format
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)


def select_pf_data(shares):
    all_data = pd.DataFrame()
    for i, s in enumerate(shares.columns):
        path = os.path.join(os.getcwd(), "data_prepared", s+'.csv')
        if os.path.exists(path):
            # print("Reading data for {}".format(s))
            new_data = pd.read_csv(path, usecols=['Date', 'RoR'], index_col='Date').rename(columns={"RoR": s})
            if i == 0:
                all_data = new_data
            else:
                all_data = pd.concat([all_data, new_data], axis=1)
            # print(all_data)
    all_data.sort_index(inplace=True)
    return all_data[all_data.index > '1990-01-01']


def simulate_pf_performance(start_balance, qtr_rors, engagement_ratio=0.5):
    # calculate initial values of invested amount
    curr_engmt_balance = start_balance * engagement_ratio
    curr_safe_balance = max(start_balance - curr_engmt_balance, 0)

    # create new data frame with initial values and index from qtr_rors
    performance_series = pd.DataFrame(columns=['ror', 'ct', 'ceb', 'csb', 'dwd', 'dwdcnt', 'dwdprc', 'yror']
                                      , index=qtr_rors.index)
    performance_series.loc['1990-01-01'] = [0, start_balance, curr_engmt_balance, curr_safe_balance, 0, 0, 0, 0]
    performance_series.sort_index(inplace=True)

    max_balance = start_balance
    drawdown_cnt = 0
    prev_yr_balance = start_balance
    for i, qtr in qtr_rors.iteritems():
        # evaluate quarter results
        curr_engmt_balance = curr_engmt_balance + curr_engmt_balance * qtr
        # re-balance portfolio
        curr_total = curr_safe_balance + curr_engmt_balance
        curr_engmt_balance = curr_total * engagement_ratio
        curr_safe_balance = max(curr_total - curr_engmt_balance, 0)

        # calculate drawdown
        max_balance = max(max_balance, curr_total)
        drawdown = max(max_balance - curr_total, 0)
        drawdown_prc = drawdown / max_balance
        if drawdown != 0:
            drawdown_cnt += 1
        else:
            drawdown_cnt = 0

        # calculate yearly return rates
        timestamp = pd.to_datetime(i)
        if timestamp.quarter == 1:
            yror = (curr_total - prev_yr_balance) / prev_yr_balance
            prev_yr_balance = curr_total
        else:
            yror = None

        # save current quarter results
        performance_series.loc[i, ['ror', 'ct', 'ceb', 'csb', 'dwd', 'dwdcnt', 'dwdprc', 'yror']] = \
            [qtr, curr_total, curr_engmt_balance, curr_safe_balance, drawdown, drawdown_cnt, drawdown_prc, yror]

    drawdown_max = performance_series['dwdprc'].max()
    drawdown_length = performance_series['dwdcnt'].max()
    ror_mean = performance_series['yror'].mean()

    return performance_series, [ror_mean, drawdown_max, drawdown_length]


def various_pf(ror_values, eng_ratios_list, start_balance=1000):
    pf_all = pd.DataFrame(columns=['ratio', 'y_ror', 'drawback', 'drawback_time'])
    different_pf_series = pd.DataFrame()
    for i, ratio in enumerate(eng_ratios_list):
        series, pf_results = simulate_pf_performance(start_balance, ror_values, engagement_ratio=ratio)
        print("results for {:.2f} ratio: {}".format(ratio, pf_results))
        pf_all.loc[len(pf_all.index)] = ["f_{:.2f}".format(ratio), pf_results[0], pf_results[1], pf_results[2]]
        pf_single = series['ct'].rename('f_{:.2f}'.format(ratio))
        if i == 0:
            different_pf_series = pf_single
        else:
            different_pf_series = pd.concat([pf_single, different_pf_series], axis=1)

    return pf_all, different_pf_series


pf_shares = pd.read_csv(os.path.join(os.getcwd(), "model_results", 'dual_values_tstudent.csv'),
                        usecols=['tickers', 'dual_values']).transpose()
pf_shares.columns = list(pf_shares.iloc[0])
pf_shares.drop(index='tickers', inplace=True)

# portfolio_series = select_pf_data(pf_shares)
# with open(os.path.join(os.getcwd(), 'portfolio_history.csv'), 'w') as file:
#     file.write(portfolio_series.to_csv(sep=';'))

portfolio_series = pd.read_csv(os.path.join(os.getcwd(), 'portfolio_history.csv'), sep=';', index_col=0)

# calculate return rate of the portfolio based on the delivered
# historical values and portfolio share for each stock
overall_ror = zip_portfolio(portfolio_series, pf_shares)

f_values = np.sort(np.linspace(1, 0, 20, endpoint=False))
evaluation, all_pf_series = various_pf(overall_ror, f_values, start_balance=1000)
# ror_alt = (series.iloc[series.shape[0]-1]['ct'] - series.iloc[0]['ct']) / series.iloc[0]['ct']
# print((1+ror_alt)**(1/29) - 1)

# save evaluation results to file
with open(os.path.join(os.getcwd(), "pf_evaluation.csv"), 'w') as file:
    file.write(evaluation.to_csv(sep=';'))

evaluation.sort_index(ascending=False, inplace=True)
for index, row in evaluation.iterrows():
    plot = plt.scatter(x=row['y_ror'], y=row['drawback'], label=row['ratio'], s=14)

plt.gca().set_yticklabels(['{:.0f}%'.format(y*100) for y in plt.gca().get_yticks()])
plt.gca().set_xticklabels(['{:.0f}%'.format(x*100) for x in plt.gca().get_xticks()])
plt.grid(1)
plt.xlabel('mean yearly rate of return')
plt.ylabel('max drawback')
plt.legend()
plt.show()


# for i, c in enumerate(all_pf_series.columns.values):
#     plt.plot(all_pf_series.index.values, all_pf_series[c], label=all_pf_series.iloc[:, i].name)
# plt.xticks(all_pf_series.index.values[0::12])
# plt.grid(1)
# plt.legend()
# plt.show()
