import os
import pandas as pd
import numpy as np


def read_worst_case_dist():
    worst_filepath = os.listdir(os.path.join(os.getcwd(), "worst_case_distributions"))
    wcd = pd.DataFrame()
    for file in worst_filepath:
        # print('reading data for: ', file)
        new_dist = pd.read_csv(os.path.join(os.getcwd(), "worst_case_distributions", file))
        # print(new_dist)
        wcd.insert(loc=len(wcd.columns), column=file.replace(".csv", ""), value=new_dist['worst'])
    return wcd


def convert_samples_ror(copula_type, distributions, annotation=''):
    samples_path = os.path.join(os.getcwd(), "copula_scenarios", copula_type+annotation+".csv")
    samples = pd.read_csv(samples_path)
    converted_samples = pd.DataFrame(np.nan, index=range(len(samples.index)), columns=samples.columns)
    if len(samples.columns) != len(distributions.columns):
        print("\nCannot convert samples: Different number of sampled stocks and supplied distributions!")
        return
    else:
        distributions = distributions.reindex(samples.columns, axis=1)
    for c in range(len(samples.columns)):
        print('converting stock: ', samples.columns[c])
        converted_samples.iloc[:, c] = distributions[distributions.columns[c]].\
            quantile(q=samples.iloc[:, c], interpolation='lower').reset_index(drop=True).to_frame()
    # write samples to the output file
    with open(os.path.join(os.getcwd(), "copula_scenarios", copula_type + annotation + '_rates.csv'), 'w') as file:
        file.write(converted_samples.to_csv(sep=";", index=False))

    return converted_samples


# read worst case distributions into 1 df
worst_case_dist = read_worst_case_dist()

# convert samples values to ror
samples_ror = convert_samples_ror("tstudent", worst_case_dist, annotation='_fh')
